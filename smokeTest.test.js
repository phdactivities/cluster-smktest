const kubeSmkTest = require("kubernetes-smktest")

// const nameSpace = "edutelling-smktest"
const nameSpace = "default"
//! Pods SmokeTest state. 

test("SmokeTExecution_unit_coverage", async () => {

    let showReport=false
    let podsState = await kubeSmkTest.statusPods(nameSpace, showReport)
    expect(podsState.passTest).toBe(true)

})


test("SmokeTest check pods logs", async () => {

    let showReport= false
    let podsState = await kubeSmkTest.logsState(nameSpace, showReport)

    expect(podsState.passTest).toBe(true)

})